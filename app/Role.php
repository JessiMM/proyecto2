<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Role extends Model implements Auditable
{
    //
    use \OwenIt\Auditing\Auditable;

  	protected $auditInclude = ['name' , 'description' ];


	protected $fillable = [ 'name' , 'description' ];


    public function users()
    {
        return $this->belongsToMany(User::class)->withTimestamps();
    }


}
