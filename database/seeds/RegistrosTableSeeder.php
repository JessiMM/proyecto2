<?php

use Illuminate\Database\Seeder;

class RegistrosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $faker = \Faker\Factory::create();
        for ($i=1; $i< 20; $i++){
            DB::table('registros')->insert([
                'nombre'=>$faker->name,
                'apeMaterno'=> $faker->lastName,
                'apePaterno'=> $faker->lastName,
                'email' => $faker->unique()->safeEmail,
                'estatus_reg'=>1
            ]);
        }
    }
}
