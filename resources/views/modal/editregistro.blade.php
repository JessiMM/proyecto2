<div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false"  data-backdrop="false" data-keyboart="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="nombreHeader">Editar</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </div>
          <div class="modal-body" id="modal_content">
            <form class="form-horizontal" role="form" id="nuevo_usuario">
              <div class="modal-body">
                <div class="form-row">
                  <div class="form-group col-md-6" style="text-align: left;">
                    <label for="nombre" >Nombre (s)</label>
                    <input type="text" class="form-control" id="nombre" name="nombre">
                  </div>
                  <div class="form-group col-md-6" style="text-align: left;">
                    <label for="apePaterno" >Apellido Paterno</label>
                    <input type="text" class="form-control" id="apePaterno" name="apePaterno">
                  </div>
                  <div class="form-group col-md-6" style="text-align: left;">
                    <label for="apeMaterno" >Apellido Materno</label>
                    <input type="text" class="form-control" id="apeMaterno" name="apeMaterno">
                  </div>
                  <div class="form-group col-md-6" style="text-align: left;">
                    <label for="email" >Correo Electronico</label>
                    <input type="text" class="form-control" id="email" name="email">
                  </div>
              </div>
              <div class="modal-footer">
                @if(@Auth::user()->hasRole( 'admin'))
                <button  type="button" id="usr_eliminar" name="usr_eliminar" class="btn btn-dark">
                  <span class="hidden-xs"> Eliminar</span>
                </button>
                @endif
                <button type="button" id="usr_editar" name="usr_editar" class="btn btn-sm btn-label-primary btn-bold" style="font-size: 1.4rem;">                   
                  <span class="hidden-xs"> Guardar</span>
                </button>
                {{ csrf_field() }}
              </div>
            </form>
          </div>
    </div>
  </div>
</div>
