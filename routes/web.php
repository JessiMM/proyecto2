<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('/', 'PagesController@inicio')->name('ini');
Route::get('/home', 'HomeController@index')->name('home');

//Protege las rutas para que solo se entre con registro
Route::group(['middleware' => 'auth'], function () {
	//Protege las rutas para que no se pueda regresar una vez que ya salio de la sesion
	Route::group(['middleware' => 'reglas'], function () {
		Route::get('lista','PagesController@listas')->name('list');
		Route::get('cambia','PagesController@cambios')->name('cambiar');
		Route::get('agrega','PagesController@agregare')->name('agregar');
		Route::get('perfil','PagesController@perfiles')->name('profile');
		Route::get('usuarios','PagesController@usuarios')->name('usuarios');
		Route::resource('subir','ImageUploadController' );
		Route::resource('regist', 'RegistroController');
		
		//rutas con JQuery
		Route::post('borrar', 'RegistroController@destroyer');
		Route::post('store', 'RegistroController@store');
		Route::post('update2', 'RegistroController@update2');
		
		//ruta para la tabla
		 Route::get('action', 'PagesController@registroList');
	});
});


Route::fallback(function() {
    return abort(404);
});
