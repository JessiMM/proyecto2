<table class="table"  id="user_table">
      <thead>
          <tr>
            <th scope="col">ID</th>
            <th scope="col">Nombre</th>
            <th scope="col">Apellido Materno</th>
            <th scope="col">Apellidos Paterno</th>
            <th scope="col">E-mail</th>
            @if(@Auth::user()->hasAnyRole(['user', 'admin']))
            <th scope="col">Cambia</th>
            <th scope="col">Cambia 2</th>
            @endif
            @if(@Auth::user()->hasRole( 'admin'))
            <th scope="col">Elimina</th>
            @endif
          </tr>
      </thead>
       @include('modal/editregistro')
      <!-- <tbody>
        @include('modal/editregistro')
      </tbody> -->
      <!-- @foreach($registro as $regis)    
      <tr>
        <th scope="row"> {{ $regis-> id}}</th>
        <td>{{$regis->nombre}}</td>
        <td> {{$regis->apePaterno}}  {{$regis->apeMaterno}} </td>
        <td>{{$regis->email}}</td>
        @if(@Auth::user()->hasAnyRole(['user', 'admin']))
        <td>
          <a href="{{ route('regist.edit', $regis->id)}}" class="btn btn-success"></a>
        </td>
        <td>
          <button type="button" data-id="{{$regis->id}}" data-nombre="{{$regis->nombre}}" data-apepaterno="{{$regis->apePaterno}}" data-apematerno="{{$regis->apeMaterno}}" data-email="{{$regis->email}}" data-toggle="modal" data-target="#edit"  class="btn btn-warning"></button>
            @include('modal/editregistro')
        </td>
        @endif
        @if(@Auth::user()->hasRole( 'admin'))
        <td>
          <form action="{{action('RegistroController@destroy', $regis->id)}}" method="post">
            {{csrf_field()}}
            <input name="_method" type="hidden" value="DELETE">
            <button class="btn btn-danger btn-xs" type="submit"><span class="glyphicon glyphicon-trash"></span></button>
          </form>          
        </td>
       @endif
      </tr> 
      @endforeach() -->
</table>