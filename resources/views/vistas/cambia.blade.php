@extends('plantilla/plantilla')


@section('parte')

    <div class="container">
      <h1 class="display-4">Cambia</h1>
      <br></br>  
      <form method="post"  action="{{route('regist.update', $registro->id)}}">
        @method('PATCH')
        {{csrf_field()}}
        <div class="form-row">
          <div class="col">
            <input type="text" class="form-control" placeholder="Nombre" name="nombre" value="{{$registro->nombre}}">
          </div>
          <div class="col">
            <input type="text" class="form-control" placeholder="Apellido Paterno" name="apePaterno" value="{{$registro->apePaterno}}">
          </div>
          <div class="col">
            <input type="text" class="form-control" placeholder="Apellido Materno" name="apeMaterno" value="{{$registro->apeMaterno}}">
          </div>
          <div class="col">
            <input type="text" class="form-control" placeholder="Email"  name ="email" value="{{$registro->email}}">
          </div>
          <br></br><br>
        </div>
        <input type="submit"  value="Actualizar" class="btn btn-outline-secondary">
        <br><br>
      </form>
    </div>

@endsection