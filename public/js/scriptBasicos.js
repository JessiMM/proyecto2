//--- funcion para gregar una nueva persona ---//
$('#input').click(function(){ 
  ( $('#nombre').get(0).value == "" ) ?   $(".text_nombre").html("Olvidó ingresar el nombre").css(" #FA5858"): $(".text_nombre").html("");
  ( $('#apePaterno').get(0).value == "" ) ?   $(".text_apePaterno").html("Olvidó ingresar el apellido paterno").css(" #FA5858"): $(".text_apePaterno").html("");
  ( $('#apeMaterno').get(0).value == "" ) ?   $(".text_apeMaterno").html("Olvidó ingresar el apellido materno").css(" #FA5858"): $(".text_apeMaterno").html("");
  ( $('#email').get(0).value == "" ) ?   $(".text_email").html("Olvidó ingresar el email").css(" #FA5858"): $(".text_email").html("");
                $.ajax({
                  type:'post',
                  url: 'store',
                  data:{
                    '_token':$('input[name=_token').val(),
                    'nombre':$('input[name=nombre').val(),
                    'apePaterno':$('input[name=apePaterno').val(),
                    'apeMaterno':$('input[name=apeMaterno').val(),
                    'email':$('input[name=email').val(),
                  },
                  success:function(data){
                    swal("Bien","Su registro se realizo satisfactoriamente","success")
                    recarga_pagina('contenedor_principal','lista');         
                  },
               });
});
//--- fin de la funcion ---//
//--- Variables ---//
var button;
var nombre;
var apePaterno;
var apeMaterno;
var email;
var id;
//--- fin de las variables ---//
//--- Recupera datos y los pinta en la modal ---//
     $('#edit').on('show.bs.modal', function (event) {
       button = $(event.relatedTarget)
       nombre = button.data('nombre')
       apePaterno = button.data('apepaterno')
       apeMaterno = button.data('apematerno')
       email = button.data('email')
       id = button.data('id')
        var modal = $(this)
        nombre = modal.find('.modal-body #nombre').val(nombre);
        apePaterno = modal.find('.modal-body #apePaterno').val(apePaterno);
        apeMaterno = modal.find('.modal-body #apeMaterno').val(apeMaterno);
        email = modal.find('.modal-body #email').val(email);
        modal.find('.modal-body #id').val(id);
     })
//--- fin de la funcion ---//
//---  funcion que hace el editar usuarios ---///   
     $('#usr_editar').click(function(){ 
         $.ajax({
           type:'post',
           url: 'update2',
           data:{
             '_token':$('input[name=_token').val(),
             'id':id,
             'nombre':nombre.val(),
             'apePaterno':apePaterno.val(),
             'apeMaterno':apeMaterno.val(),
             'email':email.val(),
           },
           success:function(data){
              Swal.fire({
               title: 'Su registro se edito satisfactoriamente',
               type: 'success'
              }).then((value) => {
                icon: "success";
              });
              recarga_pagina('contenedor_principal','lista');
            },
          });
      });
//--- fin de la funcion ---//
//--- funcion para eliminar registro ---//
 $('#usr_eliminar').click(function(){ 
      Swal.fire({
        title: 'Eliminar',
        text: "¿Desea eliminar a la persona?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#5d0923',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si Eliminar!'
      }).then((result) => {
          if (result.value){
             id = button.data('id')
             console.log(id);
            $.ajax({
              type:'post',
              url: 'borrar',
              data:{
                '_token':$('input[name=_token').val(),
                'id':id,
              },
              success:function(data){
                Swal.fire('Borrado!', 'La persona fue eliminada.','success')
               recarga_pagina('contenedor_principal','lista');
              },
            });
          }
        })
 });
//--- fin de la funcion ---//
//--- funcion que carga pagina sin refrescar ---//
  function recarga_pagina(div_contenedor,ruta,parametros){ 

    $('#'+div_contenedor).load(ruta,parametros, function(){
   
    });
  }
//--- fin de la funcion ---//
//--- funcion que ejecuta la funcion de limpia campos---//
$('#reg_nue').on ('click', function() {
            // llenadoSelect();
            limpiaCampos();
  });
//--- fin de la funcion ---//
//--- funcion para limpiar los campos del model nuevo personal---//

function limpiaCampos(){
    $("#nombre").val("");
    $("#apePaterno").val("");
    $("#apeMaterno").val("");
    $("#email").val("");

    $(".text_nombre").html("");
    $(".text_apePaterno").html("");
    $(".text_apeMaterno").html("");
    $(".text_email").html("");

}

 //--- fin de la funcion ---//
//--- funcion tabla Registro---//

$(document).ready( function () {
$('#user_table').DataTable({
           processing: true,
           serverSide: true,
           ajax: "action",
           columns: [
                    { data: 'id', name: 'id' },
                    { data: 'nombre', name: 'nombre' },
                    { data: 'apePaterno', name: 'apePaterno' },
                    { data: 'apeMaterno', name: 'apeMaterno' },
                    { data: 'email', name: 'email' },
                    { defaultContent: '<a href="{{ route("regist.edit", $registro->id)}}" class="btn btn-success"></a>'},
                    { defaultContent: ' <button type="button" data-id="{{$registro->id}}" data-nombre="{{$registro->nombre}}" data-apepaterno="{{$registro->apePaterno}}" data-apematerno="{{$registro->apeMaterno}}" data-email="{{$registro->email}}" data-toggle="modal" data-target="#edit"  class="btn btn-warning"></button>'},
                    { defaultContent: '<form action="{{action("RegistroController@destroy", $regis->id)}}" method="post">{{csrf_field()}}<input name="_method" type="hidden" value="DELETE"><button class="btn btn-danger btn-xs" type="submit"><span class="glyphicon glyphicon-trash"></span></button></form>'}
                 ]
        });
 });
 //--- fin de la funcion ---//