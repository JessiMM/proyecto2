<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use Nicolaslopezj\Searchable\SearchableTrait;

class Registro extends Model implements Auditable
{
    //
    use \OwenIt\Auditing\Auditable;
 	use SearchableTrait;

  	protected $auditInclude = ['nombre' , 'apePaterno', 'apeMaterno' , 'email', 'estatus_reg'];

    protected $fillable = [ 'nombre' , 'apePaterno', 'apeMaterno' , 'email', 'estatus_reg'];

     protected $searchable = [
        'columns' => [
            'registros.nombre'  => 10,
            
        ]
    ];
}
