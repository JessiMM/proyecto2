@extends('plantilla/plantilla')

@section('parte')

    <div class="container">
      <h1 class="display-4">Perfil</h1>
      <br><br>
      <div>
        <div class="profile-userpic" id="avatar_actual">
          <img src="{{ Storage::url($user[0]->avatar)}}" style="width: 50px; height: 50px">
        </div>
        <h2>{{ Auth::user()->name }}</h2>
        <h2>{{ Auth::user()->email }}</h2>
      <!-- droppzone imagenes -->
        <div>
          <form method="POST" action="{{route('subir.store')}}" class="dropzone" id="dropzone" enctype="multipart/form-data">
              {{ csrf_field() }}
              <div class="dz-message">
                  Arrastra o selecciona una Imagen
              </div>
          </form>
        </div>
      </div>
    </div>
    
@endsection