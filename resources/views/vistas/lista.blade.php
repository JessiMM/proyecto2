@extends('plantilla/plantilla')
 
@section('parte')
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
  <div  class="container">
      <h1 class="display-4">Lista</h1>
      <br>
    <button data-toggle="modal" data-target="#regnue"  id="reg_nue"  class="btn btn-info" >Nuevo Registro</button>
    <br></br>
    <br></br>
    @include('modal/nuevoregistro')
    <div id="contenedor_principal">
      <div  id="table_data">
          @include('vistas/tabla_registros')
      </div> 
    </div>
    <!-- paginacion metodo  de pages controler -->
  
  </div>



@endsection
