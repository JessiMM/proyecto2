@extends('plantilla/plantilla')


@section('parte')

    <div class="container">
      <h1 class="display-4">Agrega</h1>
      <br></br>

      <form method="POST" action="{{action('RegistroController@store' )}}"  role="form">

        {{ csrf_field() }}
        <div class="form-row">
          <div class="col">
            <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Nombre">
          </div>
          <div class="col">
            <input type="text" class="form-control" id="apellidos" name="apePaterno" placeholder="Apellido Paterno">
          </div>
          <div class="col">
            <input type="text" class="form-control" id="apellidos" name="apeMaterno" placeholder="Apellido Materno">
          </div>
          <div class="col">
            <input type="text" class="form-control" id="email" name="email" placeholder="Email">
          </div><br></br> <br>
        </div>
        
        <button type="submit" class="btn btn-outline-secondary">Agrega</button>
      </form> 
    </div>

@endsection
