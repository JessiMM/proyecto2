<div class="modal fade" id="regnue" tabindex="-1"  role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="true" data-keyboart="false">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="myModalLabel" >Nuevo Usuario</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </div>
            <div class="modal-body" id="modal_content">
              <form class="form-horizontal" role="form" id="nuevo_usuario">
          				<div class="modal-body">
                    <div class="form-row">
            					<div class="form-group col-md-6" style="text-align: left;">
            						<label  for="nombre">Nombre (s)</label>
            						<input type="text" class="form-control" id="nombre" name="nombre" required>
                        <strong class="text_nombre"></strong>
            					</div>
                      <div class="form-group col-md-6" style="text-align: left;">
                        <label for="apePaterno"  >Apellido Paterno</label>
                        <input type="text" class="form-control" id="apePaterno" name="apePaterno" required>
                        <strong class="text_apePaterno"></strong>
                      </div>
                      <div class="form-group col-md-6" style="text-align: left;">
                        <label for="apeMaterno">Apellido Materno</label>
                          <input type="text" class="form-control" id="apeMaterno" name="apeMaterno" required>
                            <strong class="text_apeMaterno"></strong>
                      </div>    
                      <div class="form-group col-md-6" style="text-align: left;">
                        <label for="email" class="control-label">Correo Electronico</label>
                        <input type="text" class="form-control" id="email" name="email">
                          <strong class="text_email"></strong>
                      </div>
                    </div>
                  </div>
            			<div class="modal-footer" >
            				<button type="button" id="input" name="input" class="btn btn-sm btn-label-primary btn-bold">
            				<span class="fa fa-save"></span>
                    <span class="hidden-xs"> Guardar</span>
            				</button>   {{ csrf_field() }}
            			</div>
    			    </form>
            </div>
        </div>
    </div>
</div>