<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $faker = \Faker\Factory::create();
        for ($i=1; $i< 5; $i++){
          DB::table('users')->insert([
            'name'=> $faker->name,
            'email' => $faker->unique()->safeEmail,
            'password' =>bcrypt('12345678'),
            'avatar' => 'avatar.png',
            'estatus_usu'=>1
          ]);
          DB::table('role_user')->insert([
            'role_id'=> random_int(1,2),
            'user_id'=> $i
          ]);

        }

    }
}
