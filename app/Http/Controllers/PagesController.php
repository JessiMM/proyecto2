<?php

namespace App\Http\Controllers;
use Illuminate\Contracts\Support\Jsonable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Redirect,Response,DB,Config;
use Datatables;
use App\Registro;
use App\Role;
use App\User;

class PagesController extends Controller
{
    public function inicio(){
    	
    	return view('vistas/index');
    }

    public function listas(){
        $registro = Registro::from('registros')
            ->select('registros.*')
            ->where('registros.estatus_reg', '=', 1)->latest()->paginate();
        
    	return view('vistas/lista')->with('registro',$registro);
    }
    // Funcion para tabla Registros
    public function registroList(Request $request)
    {
         $registro = DB::table('registros')->select('*');
        return datatables()->of($registro)
            ->make(true);
    }

    public function cambios(){
    	$registro = Registro::all();
    	return view('vistas/cambia',compact('registro'));
    }
    
    public function agregare(){
    	$registro = Registro::all();
    	return view('vistas/agrega',compact('registro'));
    }

    public function perfiles(){
        $id = Auth::user()->id;
        $user = User::from('users')
                        ->select('users.*')
                        ->where('users.id', '=', $id)->get();

        $roles = Role::all();
        return view('vistas/perfil',compact('roles'))->with('user',$user);
    }

    public function usuarios(){
        $roles = Role::all();
        return view('vistas/control_usuarios',compact('roles'));
    }
}


