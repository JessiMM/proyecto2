<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Registro;
use App\Role;
use App\User;

class RegistroController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $registro = new Registro();
        // $registro->id = auth()->id();
        $registro->nombre = $request->nombre;
        $registro->apePaterno = $request->apePaterno;
        $registro->apeMaterno = $request->apeMaterno;
        $registro->email = $request->email;
        
        $registro-> save();
        return redirect()->route('list');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resoue.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $registro = Registro::findOrFail($id);

        return view('vistas/cambia', compact('registro'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $request->validate([
        'nombre'=>'required',
        'apePaterno'=> 'required',
        'apeMaterno'=> 'required',
        'email' => 'required'
         ]);

        $registro = Registro::find($id);
        $registro->nombre = $request->get('nombre');
        $registro->apePaterno = $request->get('apePaterno');
        $registro->apeMaterno = $request->get('apeMaterno');
        $registro->email = $request->get('email');
        $registro->save();

        return redirect()->route('list');
    }
    public function update2(Request $request)
    {
        //actualiza registro Js
        $usuarios = Registro::find($request->id);
        $usuarios->nombre = $request->nombre;
        $usuarios->apePaterno = $request->apePaterno;
        $usuarios->apeMaterno = $request->apeMaterno;
        $usuarios->email = $request->email;
       $usuarios->save();

     
     }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        //
        //funcion para borrar de la base de datos
        // Registro::find($id)->delete();
        // return redirect()->route('list');

        //función para cambiar estatus del registro
        $regis  = Registro::find($id);
        $regis->estatus_reg = 0;
        $regis->save();
        return redirect()->route('list');
    }

        public function destroyer(Request $request)
    {
        //funcion cambia status de los Registros para el JS
        $regis  = Registro::find($request->id);
        $regis->estatus_reg = 0;
        $regis->save();
        return redirect()->route('list');
    }
}
